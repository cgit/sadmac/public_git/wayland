/*
 * Copyright © 2008 Kristian Høgsberg
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "wayland-util.h"

/**
 * Initialize a wl_list object.
 *
 * list: Already-allocated wl_list.
 **/
WL_EXPORT void
wl_list_init(struct wl_list *list)
{
	list->prev = list;
	list->next = list;
}

/**
 * Add a new element to a wl_list. The new element will appear just after
 * `list`.
 *
 * list: Head node of list to add to.
 * elm: Element we are adding.
 **/
WL_EXPORT void
wl_list_insert(struct wl_list *list, struct wl_list *elm)
{
	elm->prev = list;
	elm->next = list->next;
	list->next = elm;
	elm->next->prev = elm;
}

/**
 * Remove `elm` from the list containing it. `elm` itself will still point into
 * the list and bad things will happen if you reference it!.
 *
 * elm: Element to remove.
 **/
WL_EXPORT void
wl_list_remove(struct wl_list *elm)
{
	elm->prev->next = elm->next;
	elm->next->prev = elm->prev;
}

/**
 * Find the number of elements in `list`. The head node is not counted.
 *
 * list: Head element in list to count.
 **/
WL_EXPORT int
wl_list_length(struct wl_list *list)
{
	struct wl_list *e;
	int count;

	count = 0;
	e = list->next;
	while (e != list) {
		e = e->next;
		count++;
	}

	return count;
}

/**
 * Determine if `list` is empty.
 *
 * list: Head node of list to check.
 **/
WL_EXPORT int
wl_list_empty(struct wl_list *list)
{
	return list->next == list;
}

/**
 * Initialize a wl_array.
 *
 * array: Array to initialize.
 **/
WL_EXPORT void
wl_array_init(struct wl_array *array)
{
	memset(array, 0, sizeof *array);
}

/**
 * Free any memory associated with a wl_array.
 *
 * array: Array to release.
 **/
WL_EXPORT void
wl_array_release(struct wl_array *array)
{
	free(array->data);
}

/**
 * Add space for one more element of size `size` to a wl_array.
 *
 * array: Array to add to.
 * size: Size of each element in `array`
 **/
WL_EXPORT void *
wl_array_add(struct wl_array *array, int size)
{
	int alloc;
	void *data, *p;

	if (array->alloc > 0)
		alloc = array->alloc;
	else
		alloc = 16;

	while (alloc < array->size + size)
		alloc *= 2;

	if (array->alloc < alloc) {
		if (array->alloc > 0)
			data = realloc(array->data, alloc);
	        else
			data = malloc(alloc);

		if (data == NULL)
			return 0;
		array->data = data;
		array->alloc = alloc;
	}

	p = array->data + array->size;
	array->size += size;

	return p;
}
