/*
 * Copyright © 2008 Kristian Høgsberg
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#include <stddef.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <assert.h>
#include "wayland.h"
#include "wayland-util.h"

/**
 * An event loop. Contains the internal state of a standard single-thread
 * looping daemon.
 *
 * epoll_fd: Epoll file descriptor used to wait for new work to come in.
 * idle_list: List of tasks that should be dispatched as soon as the loop
 * 	becomes idle.
 **/
struct wl_event_loop {
	int epoll_fd;
	struct wl_list idle_list;
};

/**
 * Set of standard methods for operating on a wl_event_source.
 *
 * dispatch: Function to dispatch the given event source.
 * remove: Function called when given event source is removed from the loop.
 **/
struct wl_event_source_interface {
	void (*dispatch)(struct wl_event_source *source,
			 struct epoll_event *ep);
	int (*remove)(struct wl_event_source *source);
};

/**
 * A source of events to be handled within the Wayland event loop.
 *
 * This struct is polymorphic, so other structs may use it as a base.
 *
 * interface: Methods for handling this particular type of event.
 * loop: The event loop this source belongs to.
 **/
struct wl_event_source {
	struct wl_event_source_interface *interface;
	struct wl_event_loop *loop;
};

/**
 * A wl_event_source for file descriptor event sources.
 *
 * base: The base wl_event_source
 * fd: File descriptor that generates events.
 * func: Higher-level dispatching function specific to file descriptors.
 * data: Misc. data passed to `func`.
 **/
struct wl_event_source_fd {
	struct wl_event_source base;
	int fd;
	wl_event_loop_fd_func_t func;
	void *data;
};

/**
 * Dispatch method for wl_event_source_fd type wl_event_sources.
 *
 * source: Event source being dispatched.
 * epoll_event: Event from epoll causing the dispatch.
 **/
static void
wl_event_source_fd_dispatch(struct wl_event_source *source,
			    struct epoll_event *ep)
{
	struct wl_event_source_fd *fd_source = (struct wl_event_source_fd *) source;
	uint32_t mask;

	mask = 0;
	if (ep->events & EPOLLIN)
		mask |= WL_EVENT_READABLE;
	if (ep->events & EPOLLOUT)
		mask |= WL_EVENT_WRITEABLE;

	fd_source->func(fd_source->fd, mask, fd_source->data);
}

/**
 * Remove method for wl_event_source_fd type wl_event_sources.
 *
 * source: Event source being dispatched.
 **/
static int
wl_event_source_fd_remove(struct wl_event_source *source)
{
	struct wl_event_source_fd *fd_source =
		(struct wl_event_source_fd *) source;
	struct wl_event_loop *loop = source->loop;
	int fd;

	fd = fd_source->fd;
	free(source);

	return epoll_ctl(loop->epoll_fd, EPOLL_CTL_DEL, fd, NULL);
}

/**
 * Interface for wl_event_source_fd type wl_event_sources.
 **/
struct wl_event_source_interface fd_source_interface = {
	wl_event_source_fd_dispatch,
	wl_event_source_fd_remove
};

/**
 * Add a file descriptor to an event loop as an event source.
 *
 * loop: Loop to add to.
 * fd: File descriptor to add.
 * mask: Bitfield taken from WL_EVENT_READABLE and WL_EVENT_WRITEABLE.
 * 	Indicates types of states to watch `fd` for.
 * func: Handler function called when an event occurs.
 * data: Misc. data passed to `func`.
 **/
WL_EXPORT struct wl_event_source *
wl_event_loop_add_fd(struct wl_event_loop *loop,
		     int fd, uint32_t mask,
		     wl_event_loop_fd_func_t func,
		     void *data)
{
	struct wl_event_source_fd *source;
	struct epoll_event ep;

	source = malloc(sizeof *source);
	if (source == NULL)
		return NULL;

	source->base.interface = &fd_source_interface;
	source->base.loop = loop;
	source->fd = fd;
	source->func = func;
	source->data = data;

	ep.events = 0;
	if (mask & WL_EVENT_READABLE)
		ep.events |= EPOLLIN;
	if (mask & WL_EVENT_WRITEABLE)
		ep.events |= EPOLLOUT;
	ep.data.ptr = source;

	if (epoll_ctl(loop->epoll_fd, EPOLL_CTL_ADD, fd, &ep) < 0) {
		free(source);
		return NULL;
	}

	return &source->base;
}

/**
 * Change what events a file descriptor event source is listening for.
 *
 * source: Source to alter.
 * mask: ORing of WL_EVENT_READABLE and WL_EVENT_WRITEABLE to indicate whether
 * 	readability or writeability should be listened for.
 **/
WL_EXPORT int
wl_event_source_fd_update(struct wl_event_source *source, uint32_t mask)
{
	struct wl_event_source_fd *fd_source =
		(struct wl_event_source_fd *) source;
	struct wl_event_loop *loop = source->loop;
	struct epoll_event ep;

	ep.events = 0;
	if (mask & WL_EVENT_READABLE)
		ep.events |= EPOLLIN;
	if (mask & WL_EVENT_WRITEABLE)
		ep.events |= EPOLLOUT;
	ep.data.ptr = source;

	return epoll_ctl(loop->epoll_fd,
			 EPOLL_CTL_MOD, fd_source->fd, &ep);
}

/**
 * A wl_event_source for timer events.
 *
 * base: The base wl_event_source.
 * fd: Timer file descriptor.
 * func: Higher-level dispatch function for timer events.
 * data: Misc. data passed to `func`.
 **/
struct wl_event_source_timer {
	struct wl_event_source base;
	int fd;
	wl_event_loop_timer_func_t func;
	void *data;
};

/**
 * Dispatch method for timer event sources.
 *
 * source: The timer event source to dispatch.
 * ep: Epoll event for the timer file descriptor.
 **/
static void
wl_event_source_timer_dispatch(struct wl_event_source *source,
			       struct epoll_event *ep)
{
	struct wl_event_source_timer *timer_source =
		(struct wl_event_source_timer *) source;
	uint64_t expires;

	read(timer_source->fd, &expires, sizeof expires);

	timer_source->func(timer_source->data);
}

/**
 * Remove method for timer event sources.
 *
 * source: The timer event source to remove.
 **/
static int
wl_event_source_timer_remove(struct wl_event_source *source)
{
	struct wl_event_source_timer *timer_source =
		(struct wl_event_source_timer *) source;
	struct wl_event_loop *loop = source->loop;
	int fd;

	fd = timer_source->fd;
	free(source);

	return epoll_ctl(loop->epoll_fd, EPOLL_CTL_DEL, fd, NULL);
}

/**
 * Interface for wl_event_source_timers
 **/
struct wl_event_source_interface timer_source_interface = {
	wl_event_source_timer_dispatch,
	wl_event_source_timer_remove
};

/**
 * Add a timer to an event loop as an event source.
 *
 * loop: Loop to add timer to.
 * func: Dispatch method to call when the timer fires.
 * data: Misc. data passed to `func`.
 **/
WL_EXPORT struct wl_event_source *
wl_event_loop_add_timer(struct wl_event_loop *loop,
			wl_event_loop_timer_func_t func,
			void *data)
{
	struct wl_event_source_timer *source;
	struct epoll_event ep;

	source = malloc(sizeof *source);
	if (source == NULL)
		return NULL;

	source->base.interface = &timer_source_interface;
	source->base.loop = loop;

	source->fd = timerfd_create(CLOCK_MONOTONIC, 0);
	if (source->fd < 0) {
		fprintf(stderr, "could not create timerfd\n: %m");
		free(source);
		return NULL;
	}

	source->func = func;
	source->data = data;

	ep.events = EPOLLIN;
	ep.data.ptr = source;

	if (epoll_ctl(loop->epoll_fd, EPOLL_CTL_ADD, source->fd, &ep) < 0) {
		free(source);
		return NULL;
	}

	return &source->base;
}

/**
 * Update the expiration time of a timer event source.
 *
 * source: Timer event source to update.
 * ms_delay: Delay in miliseconds between fires.
 **/
WL_EXPORT int
wl_event_source_timer_update(struct wl_event_source *source, int ms_delay)
{
	struct wl_event_source_timer *timer_source =
		(struct wl_event_source_timer *) source;
	struct itimerspec its;

	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 0;
	its.it_value.tv_sec = 0;
	its.it_value.tv_nsec = ms_delay * 1000 * 1000;
	if (timerfd_settime(timer_source->fd, 0, &its, NULL) < 0) {
		fprintf(stderr, "could not set timerfd\n: %m");
		return -1;
	}

	return 0;
}

/**
 * A wl_event_source for *nix signals.
 *
 * base: The base wl_event_source.
 * fd: Signalfd file descriptor.
 * signal_number: The signal number to watch for.
 * func: Higher-level dispatch function for signal events.
 * data: Misc. data passed to `func`.
 **/
struct wl_event_source_signal {
	struct wl_event_source base;
	int fd;
	int signal_number;
	wl_event_loop_signal_func_t func;
	void *data;
};

/**
 * Dispatch method for signal event sources.
 *
 * source: The signal event source to dispatch.
 * ep: Epoll event for the signalfd file descriptor.
 **/
static void
wl_event_source_signal_dispatch(struct wl_event_source *source,
			       struct epoll_event *ep)
{
	struct wl_event_source_signal *signal_source =
		(struct wl_event_source_signal *) source;
	struct signalfd_siginfo signal_info;

	read(signal_source->fd, &signal_info, sizeof signal_info);

	signal_source->func(signal_source->signal_number, signal_source->data);
}

/**
 * Remove method for signal event sources.
 *
 * source: The signal event source to remove.
 **/
static int
wl_event_source_signal_remove(struct wl_event_source *source)
{
	struct wl_event_source_signal *signal_source =
		(struct wl_event_source_signal *) source;
	struct wl_event_loop *loop = source->loop;
	int fd;

	fd = signal_source->fd;
	free(source);

	return epoll_ctl(loop->epoll_fd, EPOLL_CTL_DEL, fd, NULL);
}

/**
 * Interface for wl_event_source_signals
 **/
struct wl_event_source_interface signal_source_interface = {
	wl_event_source_signal_dispatch,
	wl_event_source_signal_remove
};

/**
 * Make a signal an event source in an  event loop.
 *
 * loop: Loop to add to.
 * signal_number: Signal to use.
 * func: Dispatch method to call when the signal fires.
 * data: Misc. data passed to `func`.
 **/
WL_EXPORT struct wl_event_source *
wl_event_loop_add_signal(struct wl_event_loop *loop,
			int signal_number,
			wl_event_loop_signal_func_t func,
			void *data)
{
	struct wl_event_source_signal *source;
	struct epoll_event ep;
	sigset_t mask;

	source = malloc(sizeof *source);
	if (source == NULL)
		return NULL;

	source->base.interface = &signal_source_interface;
	source->base.loop = loop;

	sigemptyset(&mask);
	sigaddset(&mask, signal_number);
	source->fd = signalfd(-1, &mask, 0);
	if (source->fd < 0) {
		fprintf(stderr, "could not create fd to watch signal\n: %m");
		free(source);
		return NULL;
	}
	sigprocmask(SIG_BLOCK, &mask, NULL);

	source->func = func;
	source->data = data;

	ep.events = EPOLLIN;
	ep.data.ptr = source;

	if (epoll_ctl(loop->epoll_fd, EPOLL_CTL_ADD, source->fd, &ep) < 0) {
		free(source);
		return NULL;
	}

	return &source->base;
}

/**
 * A wl_event_source that is dispatched as soon as the loop has no more work to
 * do, and is then destroyed.
 *
 * base: The base wl_event_source.
 * link: Links this source into a list of idle events inside the wl_event_loop.
 * func: Higher-level dispatch function for signal events.
 * data: Misc. data passed to `func`.
 **/
struct wl_event_source_idle {
	struct wl_event_source base;
	struct wl_list link;
	wl_event_loop_idle_func_t func;
	void *data;
};

/**
 * Dispatch method for wl_event_source_idle event sources. Currently always
 * throws an error since the dispatch method is circumvented for idle events.
 **/
static void
wl_event_source_idle_dispatch(struct wl_event_source *source,
			      struct epoll_event *ep)
{
	assert(0);
}

/**
 * Remove an idle event source from its event loop.
 * 
 * source: Source to remove.
 **/
static int
wl_event_source_idle_remove(struct wl_event_source *source)
{
	struct wl_event_source_idle *idle_source =
		(struct wl_event_source_idle *) source;

	wl_list_remove(&idle_source->link);

	return 0;
}

/**
 * Interface for wl_event_source_idle event sources.
 **/
struct wl_event_source_interface idle_source_interface = {
	wl_event_source_idle_dispatch,
	wl_event_source_idle_remove
};

/**
 * Set up `func` to be called the next time `loop` exhausts its workload and
 * prepares to sleep.
 *
 * loop: Loop to call `func`.
 * func: Function to be called when `loop` goes idle.
 * data: Misc. data to pass to `func`.
 **/
WL_EXPORT struct wl_event_source *
wl_event_loop_add_idle(struct wl_event_loop *loop,
		       wl_event_loop_idle_func_t func,
		       void *data)
{
	struct wl_event_source_idle *source;

	source = malloc(sizeof *source);
	if (source == NULL)
		return NULL;

	source->base.interface = &idle_source_interface;
	source->base.loop = loop;

	source->func = func;
	source->data = data;
	wl_list_insert(loop->idle_list.prev, &source->link);

	return &source->base;
}

/**
 * Remove an event source from the event loop it is in.
 *
 * source: Source to remove.
 **/
WL_EXPORT int
wl_event_source_remove(struct wl_event_source *source)
{
	source->interface->remove(source);

	return 0;
}

/**
 * Create and return a new wl_event_loop.
 **/
WL_EXPORT struct wl_event_loop *
wl_event_loop_create(void)
{
	struct wl_event_loop *loop;

	loop = malloc(sizeof *loop);
	if (loop == NULL)
		return NULL;

	loop->epoll_fd = epoll_create(16);
	if (loop->epoll_fd < 0) {
		free(loop);
		return NULL;
	}
	wl_list_init(&loop->idle_list);

	return loop;
}

/**
 * Destroy a wl_event_loop.
 *
 * loop: Loop to destroy.
 **/
WL_EXPORT void
wl_event_loop_destroy(struct wl_event_loop *loop)
{
	close(loop->epoll_fd);
	free(loop);
}

/**
 * Dispatch any idle event sources for an event loop.
 *
 * loop: Loop to dispatch idles for.
 **/
static void
dispatch_idles(struct wl_event_loop *loop)
{
	struct wl_event_source_idle *source, *next;

	source = container_of(loop->idle_list.next,
			      struct wl_event_source_idle, link);

	while (&source->link != &loop->idle_list) {
		source->func(source->data);
		next = container_of(source->link.next,
				    struct wl_event_source_idle, link);
		free(source);
		source = next;
	}

	wl_list_init(&loop->idle_list);
}

/**
 * Block until `loop` has some pending work, then perform that work.
 *
 * loop: Loop to block and dispatch.
 **/
WL_EXPORT int
wl_event_loop_wait(struct wl_event_loop *loop)
{
	struct epoll_event ep[32];
	struct wl_event_source *source;
	int i, count, timeout;

	if (wl_list_empty(&loop->idle_list))
		timeout = -1;
	else
		timeout = 0;

	count = epoll_wait(loop->epoll_fd, ep, ARRAY_LENGTH(ep), timeout);
	if (count < 0)
		return -1;

	for (i = 0; i < count; i++) {
		source = ep[i].data.ptr;
		source->interface->dispatch(source, &ep[i]);
	}

	if (count == 0)
		dispatch_idles(loop);

	
	return 0;
}
