/*
 * Copyright © 2008 Kristian Høgsberg
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#ifndef WAYLAND_UTIL_H
#define WAYLAND_UTIL_H

#include <inttypes.h>

/* GCC visibility */
#if defined(__GNUC__) && __GNUC__ >= 4
#define WL_EXPORT __attribute__ ((visibility("default")))
#else
#define WL_EXPORT
#endif

/**
 * Number of elements in a static array.
 *
 * a: Array to find length of.
 **/
#define ARRAY_LENGTH(a) (sizeof (a) / sizeof (a)[0])

/**
 * Round `n` to the next `a`.
 *
 * n: Value to round.
 * a: Degree to round to. Must be a power of 2.
 **/
#define ALIGN(n, a) ( ((n) + ((a) - 1)) & ~((a) - 1) )

/**
 * Divide `n` by `a`, rounding up, rather than truncating as in integer
 * division.
 *
 * n: Dividend.
 * a: Divisor.
 **/
#define DIV_ROUNDUP(n, a) ( ((n) + ((a) - 1)) / (a) )

/**
 * Get a pointer to a struct of type `type` which has a member `member`
 * pointed to by `ptr`.
 *
 * type: Type of struct to return.
 * member: Member within the struct we have the address of.
 * ptr: Pointer to `member` within the struct we want to return.
 **/
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})

/**
 * FIXME: This appears to be unused.
 **/
struct wl_argument {
	uint32_t type;
	void *data;
};

/**
 * A name and signature for a method or event in a wl_interface.
 *
 * The signature is a string with each character representing one argument and
 * denoting type:
 * 	u: Unsigned 32-bit int.
 * 	i: Signed 32-bit int.
 * 	s: String.
 * 	o: Object (by ID).
 * 	n: New object. (TODO: Clarify this. Its not quite right.)
 * 	a: Array.
 *
 * name: Name of this message.
 * signature: What arguments this message comes with.
 * types: FIXME: This appears to be unused.
 **/
struct wl_message {
	const char *name;
	const char *signature;
	const void **types;
};

/**
 * An interface implemented by an object. Lists what operations may be
 * performed on that object.
 *
 * name: Name of this interface.
 * version: Version of this interface.
 * method_count: Length of `methods`.
 * methods: List of methods this object supports.
 * event_count: Length of `events`.
 * events: Events this object may emit which others may listen for.
 **/
struct wl_interface {
	const char *name;
	int version;
	int method_count;
	const struct wl_message *methods;
	int event_count;
	const struct wl_message *events;
};

/**
 * An object in the OO sense. It has methods specified by `interface` and
 * implemented with the functions in `implementation`, and a unique `id`.
 *
 * interface: The interface that this object adhere's to.
 * implementation: Functions implementing `interface`
 * id: A unique id.
 **/
struct wl_object {
	const struct wl_interface *interface;
	void (**implementation)(void);
	uint32_t id;
};

struct wl_hash_table;
struct wl_hash_table *wl_hash_table_create(void);
void wl_hash_table_destroy(struct wl_hash_table *ht);
void *wl_hash_table_lookup(struct wl_hash_table *ht, uint32_t hash);
int wl_hash_table_insert(struct wl_hash_table *ht, uint32_t hash, void *data);
void wl_hash_table_remove(struct wl_hash_table *ht, uint32_t hash);


/**
 * Standard doubly-linked list node.
 *
 * prev: Previous entry.
 * next: Next entry.
 **/
struct wl_list {
	struct wl_list *prev;
	struct wl_list *next;
};

void wl_list_init(struct wl_list *list);
void wl_list_insert(struct wl_list *list, struct wl_list *elm);
void wl_list_remove(struct wl_list *elm);
int wl_list_length(struct wl_list *list);
int wl_list_empty(struct wl_list *list);

/**
 * Get a void* pointer which precedes `ptr` in address space by the offset of
 * `member` within `type`.
 *
 * ptr: Base pointer.
 * sample: Struct to calculate offset relative to.
 * member: Member within struct to find offset of.
 **/
#define __container_of(ptr, sample, member)				\
	(void *)((char *)(ptr)	-					\
		 ((char *)&(sample)->member - (char *)(sample)))

/**
 * Iterate through a series of structures linked by a wl_list, starting at (but
 * not including) `head`, where `member` is a wl_list member of `pos`. At each
 * iteration of the loop, `pos` will point to the current entry.
 *
 * pos: Variable to store current entry in for each iteration.
 * head: Head of list.
 * member: Member of `pos` linking it to the list.
 **/
#define wl_list_for_each(pos, head, member)				\
	for (pos = __container_of((head)->next, pos, member);		\
	     &pos->member != (head);					\
	     pos = __container_of(pos->member.next, pos, member))

/**
 * A dynamically allocated array.
 *
 * size: Number of elements in the array.
 * alloc: Amount of space allocated for the array.
 * data: Contents of the array.
 **/
struct wl_array {
	uint32_t size;
	uint32_t alloc;
	void *data;
};

void wl_array_init(struct wl_array *array);
void wl_array_release(struct wl_array *array);
void *wl_array_add(struct wl_array *array, int size);

#endif
